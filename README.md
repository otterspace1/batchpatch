# Batch Patch

Master branch: [![pipeline status](https://gitlab.com/otterspace1/batchpatch/badges/master/pipeline.svg)](https://gitlab.com/otterspace1/batchpatch/-/commits/master)

Dev branch: [![pipeline status](https://gitlab.com/otterspace1/batchpatch/badges/master/pipeline.svg)](https://gitlab.com/otterspace1/batchpatch/-/commits/master)

The goal of this project is to provide easy to use and efficient tool to remove dust and other artifacts (like the light spots) from the already-taken photos in a batch mode.

## Task formalization

Formal task could be found here in Wiki (in Russian): [Постановка задачи](https://gitlab.com/otterspace1/batchpatch/-/wikis/Постановка-задачи)

User stories coulde be found here (in Russian): [User stories](https://gitlab.com/otterspace1/batchpatch/-/wikis/User-stories)

TODO: user stories should me migrated and tracked in [Requirements](https://gitlab.com/otterspace1/batchpatch/-/requirements). New stories accepted only from there.

## Usage

//TODO place simple usage example here (gif = before/after).

Full user documentation could be found in [Wiki](https://gitlab.com/otterspace1/batchpatch/-/wikis/Руководство-пользователя)

## Downloading

Pre-built binaries available for Windows (x32, works for x64 also) and debian-based linux (x64 only). 

Latest stable version could be retrieved from [Releases page](https://gitlab.com/otterspace1/batchpatch/-/releases).


## Coding/contribution rules

Coding, contribution and branching rules explained [here](https://gitlab.com/otterspace1/batchpatch/-/wikis/home#регламент-работы) (Russian only, should be translated)

## License

Project is licensed under Apache License 2.0. [Full text](LICENSE).