# FROM openjdk:8-jdk-stretch
# Unable to install openjfx for java 8 in debian, alpine and openjdk images, openjfx 11 is forced there, incompatible with java 8
FROM ubuntu:xenial

RUN apt-get update && apt-get install -y openjdk-8-jdk openjfx maven \
	&& rm -rf /var/lib/apt/lists/*
