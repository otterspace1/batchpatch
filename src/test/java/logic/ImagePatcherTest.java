package logic;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.fail;

class ImagePatcherTest
{
    private static ImagePatcher testing;

    @BeforeAll
    static void init()
    {
        ArrayList<Selection> selections = new ArrayList<>();
        testing = new ImagePatcher(selections);
    }

    @Test
    void doPatch()
    {
    }

    @Test
    void addPoint()
    {
        Polygon polygon = new Polygon();
        Point point = new Point(7,9);

        testing.addPoint(polygon, point);
        Assertions.assertEquals(polygon.xpoints[0], 7);
        Assertions.assertEquals(polygon.ypoints[0], 9);
    }

    @Test
    void copyImage()
    {
        BufferedImage testImage = new BufferedImage(100,100,BufferedImage.TYPE_INT_RGB);
        fillImage(testImage);
        BufferedImage copiedImage = testing.copyImage(testImage);

        Assertions.assertEquals(testImage.getWidth(), copiedImage.getWidth());
        Assertions.assertEquals(testImage.getHeight(), copiedImage.getHeight());

        for(int i =0;i < testImage.getWidth();i++)
            for(int j = 0; j < testImage.getHeight(); j++)
                Assertions.assertEquals(testImage.getRGB(i,j), copiedImage.getRGB(i,j));

         copiedImage.setRGB(50,50, copiedImage.getRGB(50,50)+50);
         Assertions.assertNotEquals(copiedImage.getRGB(50,50), testImage.getRGB(50,50));

    }

    void fillImage(BufferedImage image)
    {
        for(int i =0;i < image.getWidth();i++)
            for(int j = 0; j < image.getHeight(); j++)
                image.setRGB(i,j,(i+j));
    }

    @Test
    void isPointAtBound()
    {
        Polygon polygon = new Polygon();
        polygon.addPoint(0,0);
        polygon.addPoint(10,0);
        polygon.addPoint(10,10);
        polygon.addPoint(0, 10);

        Point point = new Point(0,10);
        if(!testing.isPointAtBound(polygon, point))
            fail("Failed to check point at bound");
    }

    @Test
    void isPointNotAtBound()
    {
        Polygon polygon = new Polygon();
        polygon.addPoint(0,0);
        polygon.addPoint(10,0);
        polygon.addPoint(10,10);
        polygon.addPoint(0, 10);

        Point point = new Point(5,5);
        if(testing.isPointAtBound(polygon, point))
            fail("Failed to check point not at bound");
    }

    @Test
    void normalize()
    {
        ArrayList<Color> colors = new ArrayList<>();
        colors.add(new Color(254,254,254,0));
        colors.add(new Color(127,127,127,0));
        colors.add(new Color(0f,0f,0f,0f));
        Color color = testing.normalize(colors);

        if(color.getRed() != 127 || color.getGreen() != 127 || color.getBlue()!= 127)
            fail("Failed to normalize colors");
    }
}