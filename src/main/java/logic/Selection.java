package logic;

import javafx.geometry.Point2D;
import lombok.Getter;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Selection
{
    @Getter
    private List<Point2D> vertices;
    @Getter
    private Polygon boundPixels;

    @Getter
    private Point2D lastPoint;

    public Selection(Point2D startPoint)
    {
        boundPixels = new Polygon();
        vertices = Collections.synchronizedList(new ArrayList<>());

        vertices.add(startPoint);
        lastPoint = startPoint;
    }

    /**
     * Adds point to selection
     * @param point point to be added
     * @return new edge to be drawn on the ImageView
     */
    public List<Point2D> addPoint(Point2D point)
    {
        this.vertices.add(point);
        List<Point2D> newEdge = fillLinePixelsCDA(lastPoint.getX(), lastPoint.getY(), point.getX(), point.getY());
        this.lastPoint = point;

        return newEdge;
    }

    /**
     * Closes drawn polygon
     * @return last edge to be drawn on ImageView
     */
    public List<Point2D> closeLine()
    {
        this.vertices.add(vertices.get(0));
        return fillLinePixelsCDA(lastPoint.getX(), lastPoint.getY(), vertices.get(0).getX(), vertices.get(0).getY());
    }

    /**
     * Adds dots of the outline to polygon dots
     * @param startX x-coordinate of the first point
     * @param startY y-coordinate of the first point
     * @param endX x-coordinate of the last point
     * @param endY y-coordinate of the last point
     * @return pixels of the new edge
     */
    private List<Point2D> fillLinePixelsCDA(double startX, double startY, double endX, double endY)
    {
        List<Point2D> newEdgePixels = new ArrayList<>();

        double dx = Math.abs(endX - startX);
        double dy = Math.abs(endY - startY);
        double l = dx > dy ? dx : dy;
        double sigX = (endX - startX) / l;
        double sigY = (endY - startY) / l;

        double y = startY;
        double x = startX;


        for (int i = 0; i <= l; i++)
        {
            boundPixels.addPoint((int)x,(int)y);
            newEdgePixels.add(new Point2D(x, y));

            x += sigX;
            y += sigY;
        }
        return newEdgePixels;
    }
}
