package logic;

import controller.ImageViewerController;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;
import logic.exception.ImageLoadException;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.imaging.ImageReadException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ImageViewer
{
    @Getter
    private ImageViewerController controller;
    @Getter
    private PatchableImage patchableImage;
    @Getter
    private ObjectProperty<Image> image;

    @Getter @Setter
    private ArrayList<Selection> selections;
    @Getter @Setter
    private SimpleBooleanProperty selectionEnabledProperty;

    public ImageViewer() throws IOException
    {
        this.image = new SimpleObjectProperty<>();
        this.selections = new ArrayList<>();
        this.selectionEnabledProperty = new SimpleBooleanProperty();
        controller = ImageViewerController.create();
        controller.setImageViewer(this);
    }

    /**
     * Loads image file to ImageViewer
     * @param file file of an Image to be loaded
     * @throws ImageLoadException
     */
    public void loadImageFromFile(File file) throws ImageLoadException, IOException, ImageReadException
    {
        this.controller.stopBlinkTimer();
        this.patchableImage = new PatchableImage(file);
        this.selections = new ArrayList<>();
        this.image.setValue(this.patchableImage.getDisplayableImage());
    }


    /**
     * Removes all selections from opened image
     */
    public void removeSelections()
    {
        this.selections.clear();
        this.controller.removeSelections();
    }
}
