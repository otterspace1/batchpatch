package logic;

import javafx.scene.image.Image;
import logic.exception.ImageLoadException;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.TiffTagConstants;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

public class PatchableImage
{
    @Setter @Getter
    private String name;
    //@Getter
    private Image image;
    @Getter @Setter
    private BufferedImage bufferedImage;
    @Getter
    private File sourceFile;
    @Setter @Getter
    private ImageMetadata metadata;

    @Getter
    private int orientation;

    public PatchableImage(String name)
    {
        this.name = name;
    }

    public PatchableImage(File file) throws IOException, ImageLoadException
    {
        if(file != null)
        {
            this.sourceFile = file;
            this.name = file.getName();
            try
            {
                this.bufferedImage = Imaging.getBufferedImage(sourceFile);
                //loadImage(file);
            }catch (ImageReadException e)
            {
                throw new ImageLoadException(file,"Содержимое файла - не изображение");
            }

            try
            {
                metadata = Imaging.getMetadata(file);
                setMetadata(metadata);

                orientation = ((JpegImageMetadata) metadata).findEXIFValueWithExactMatch(TiffTagConstants.TIFF_TAG_ORIENTATION).getIntValue();
            } catch (ImageReadException | IOException | NullPointerException e)
            {
                metadata = null;
                orientation = 1;
            }
        }
    }

    public Image getDisplayableImage() throws ImageLoadException
    {
        if(image == null)
            image = loadImage(sourceFile);

        return image;
    }

    public void freeMemory()
    {
        if(bufferedImage != null)
        {
            bufferedImage.getGraphics().dispose();
            bufferedImage = null;
        }

        if(image != null)
            image = null;

        System.gc();
    }

    /**
     * Loads Image in program
     * @param file Image file
     * @throws ImageLoadException
     */
    private Image loadImage(File file) throws ImageLoadException
    {
        Image img = null;
        try
        {
            img = new Image(file.toURI().toURL().toString());
        } catch (MalformedURLException e)
        {
            ImageLoadException e1 = new ImageLoadException(file, "Невозможно открыть файл");
            e1.addSuppressed(e);
            throw e1;
        }
        if(img.isError())
            throw new ImageLoadException(file, "Содержимое файла - не изображение");
        //this.image = img;

        return img;
    }

    /**
     * Calculates rotation of image in angles according to its EXIF param.
     * @return rotation of image
     */
    public int getImageRotationDegrees()
    {
        int res = 0;
        switch(orientation)
        {
            case 1:
                break;
            case 2: // Flip X
                //t.scale(-1.0, 1.0);
                //t.translate(-info.width, 0);
                break;
            case 3: // PI rotation
                //t.translate(info.width, info.height);
                //t.rotate(Math.PI);
                res = 180;
                break;
            case 4: // Flip Y
                //t.scale(1.0, -1.0);
                //t.translate(0, -info.height);
                break;
            case 5: // - PI/2 and Flip X
                //t.rotate(-Math.PI / 2);
                //t.scale(-1.0, 1.0);
                res = 90;
                break;
            case 6: // -PI/2 and -width
                //t.translate(info.height, 0);
                //t.rotate(Math.PI / 2);
                res = 90;
                break;
            case 7: // PI/2 and Flip
                //t.scale(-1.0, 1.0);
                //t.translate(-info.height, 0);
                //t.translate(0, info.width);
                //t.rotate(  3 * Math.PI / 2);
                res = -90;
                break;
            case 8: // PI / 2
                //t.translate(0, info.width);
                //t.rotate(  3 * Math.PI / 2);
                res = -90;
                break;
        }
        return res;
    }
}
