package logic;

import logic.exception.ImageDimensionException;
import logic.exception.ImageSaveException;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class FileProcessor
{
    @Getter
    private static final FileProcessor instance = new FileProcessor();


    private int threadNumber;
    private ExecutorService threadExecutor;

    @Setter
    private String resultSuffix = "_patched";
    private String tmpSuffix = "_tmp";

    private FileProcessor()
    {
        threadNumber = Runtime.getRuntime().availableProcessors();
        threadExecutor = Executors.newFixedThreadPool(threadNumber);
    }

    /**
     * Shuts down executor in FileProcessor
     * @return true - if executor is terminated, false - if the timeout elapsed before termination
     * @throws InterruptedException
     */
    public boolean deinitialise() throws InterruptedException
    {
        threadExecutor.shutdown();
        return threadExecutor.awaitTermination(2, TimeUnit.SECONDS);
    }

    /**
     * Shuts down executors forcibly
     */
    public void deinitializeForcibly()
    {
        threadExecutor.shutdownNow().forEach(runnable -> ((Future)runnable).cancel(true));
    }

    /**
     * Copies source image metadata to resulting file using temprorary image.
     * @param sourceImage unmodified image with metadata
     * @param tmpFile modified temporary image without metadata
     * @param resultingFile file, modified image with metadata will be saved to
     * @throws IOException
     * @throws ImageWriteException
     * @throws ImageReadException
     */
    private void copyImageMetadata(PatchableImage sourceImage, File tmpFile, File resultingFile) throws IOException, ImageWriteException, ImageReadException
    {
        OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(resultingFile));
        TiffOutputSet outputSet = null;

        JpegImageMetadata jpegMetadata = (JpegImageMetadata) sourceImage.getMetadata();
        if (jpegMetadata != null)
        {
            TiffImageMetadata exif = jpegMetadata.getExif();
            if (exif != null)
                outputSet = exif.getOutputSet();
        }
        new ExifRewriter().updateExifMetadataLossless(tmpFile, outputStream, outputSet);
    }

    /**
     * Saves patched image
     * @param toSave Image for saving
     * @param resultingFile File in which to write Image
     * @throws IOException
     */
    public void saveImage(BufferedImage toSave, File resultingFile) throws IOException
    {
        // Get buffered image:
        /*BufferedImage image = toSave;//SwingFXUtils.fromFXImage(toSave, null);

        // Remove alpha-channel from buffered image:
        BufferedImage imageRGB = new BufferedImage(
                image.getWidth(),
                image.getHeight(),
                BufferedImage.OPAQUE);

        Graphics2D graphics = imageRGB.createGraphics();

        graphics.drawImage(image, 0, 0, null);
        if(!resultingFile.exists())
            resultingFile.createNewFile();*/

        ImageIO.write(toSave, "jpg", resultingFile);

        //graphics.dispose();
    }

    /**
     * Patches image in the selected area
     * @param sourceImageFile initial image file
     * @param outputFolder the output directory for result
     * @param baseImage Image, opened in ImageViewer
     * @param patcher ImagePatcher to be used
     * @return Future representing pending complection of the process
     */
    Future<File> processImage(File sourceImageFile, File outputFolder, PatchableImage baseImage, ImagePatcher patcher)
    {
        return threadExecutor.submit(() ->
        {
            PatchableImage patchableImage = new PatchableImage(sourceImageFile);

            if(patchableImage.getBufferedImage().getWidth() != baseImage.getBufferedImage().getWidth()
                    || patchableImage.getBufferedImage().getHeight() != baseImage.getBufferedImage().getHeight())
                throw new ImageDimensionException(patchableImage);

            //patcher.doPatch(patchableImage);

            BufferedImage wi = patcher.doPatch(patchableImage);

            File resultingFile = outputFolder.toPath().resolve(patchableImage.getName()+resultSuffix).toFile();

            try
            {
                if(patchableImage.getMetadata() != null)
                {
                    File tmpFile = outputFolder.toPath().resolve(patchableImage.getName()+tmpSuffix).toFile();
                    FileProcessor.getInstance().saveImage(wi, tmpFile);
                    if(!resultingFile.exists())
                        resultingFile.createNewFile();
                    FileProcessor.getInstance().copyImageMetadata(patchableImage, tmpFile, resultingFile);
                    tmpFile.delete();
                }
                else
                {
                    FileProcessor.getInstance().saveImage(wi, resultingFile);
                }
            }
            catch (IOException | ImageReadException | ImageWriteException e)
            {
                ImageSaveException e1 = new ImageSaveException(patchableImage);
                e1.addSuppressed(e);
                throw e1;
            }

            patchableImage.freeMemory();
            patchableImage = null;
            System.gc();

            return resultingFile;
        });
    }

    /**
     * Patches the list of images in selected area from a selected folder
     * @param sourceFolder initial folder with images
     * @param outputFolder output directory for result
     * @param baseImage Image, opened in ImageViewer
     * @param patcher ImagePatcher to be used
     * @return List of Futures, each representing pending complection of the process
     */
    ArrayList<Future<File>> processImages(File sourceFolder, File outputFolder, PatchableImage baseImage, ImagePatcher patcher)
    {
        ArrayList<Future<File>> futures = new ArrayList<>();
        if(sourceFolder.isDirectory())
        {
            File[] files = sourceFolder.listFiles();
            if (files != null)
            {
                for (File file: files)
                {
                    if(file.isDirectory())
                        futures.addAll(processImages(file, outputFolder, baseImage, patcher));
                    else
                        futures.add(processImage(file, outputFolder, baseImage, patcher));
                }
            }
        }
        return futures;
    }

    /**
     * Patches the list of images in selected area
     * @param sourceFiles list of initial image file
     * @param outputFolder output directory for result
     * @param baseImage Image, opened in ImageViewer
     * @param patcher ImagePatcher to be used
     * @return List of Futures, each representing pending complection of the process
     */
    ArrayList<Future<File>> processImages(List<File> sourceFiles, File outputFolder, PatchableImage baseImage, ImagePatcher patcher)
    {
        ArrayList<Future<File>> futures = new ArrayList<>();

        for (File file: sourceFiles)
        {
            if(file.isDirectory())
                futures.addAll(processImages(file, outputFolder, baseImage, patcher));
            else
                futures.add(processImage(file, outputFolder, baseImage, patcher));
        }

        return futures;
    }
}
