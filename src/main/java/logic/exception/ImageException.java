package logic.exception;

import logic.PatchableImage;
import lombok.Getter;

public class ImageException extends Exception
{
    @Getter
    private PatchableImage exceptionImage;

    public ImageException(PatchableImage exceptionImage, String exceptionMessage)
    {
        super(exceptionMessage);
        this.exceptionImage = exceptionImage;
    }
}
