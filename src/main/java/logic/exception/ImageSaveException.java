package logic.exception;

import logic.PatchableImage;

public class ImageSaveException extends ImageException
{
    public ImageSaveException(PatchableImage exceptionImage)
    {
        super(exceptionImage, "Не удалось сохранить следующие изображения");
    }
}
