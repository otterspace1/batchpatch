package logic.exception;

import lombok.Getter;

import java.io.File;

public class ImageLoadException extends Exception
{
    @Getter
    private File imageFile;

    public ImageLoadException(File imageFile, String exceptionMessage)
    {
        super(exceptionMessage);
        this.imageFile = imageFile;
    }
}
