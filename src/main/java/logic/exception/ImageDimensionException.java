package logic.exception;

import logic.PatchableImage;

public class ImageDimensionException extends ImageException
{
    public ImageDimensionException(PatchableImage exceptionImage)
    {
        super(exceptionImage, "Разрешение следующих изображений не совпадает с оригиналом");
    }
}
