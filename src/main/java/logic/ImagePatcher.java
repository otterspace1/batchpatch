package logic;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;


@RequiredArgsConstructor
public class ImagePatcher
{
    @NonNull
    private ArrayList<Selection> selections;

    /**
     * Patches the selected area
     * @param toPatch Image to be patched
     * @return patched Image
     */
    public BufferedImage doPatch(PatchableImage toPatch)
    {
        int width = toPatch.getBufferedImage().getWidth();
        int height = toPatch.getBufferedImage().getHeight();

        BufferedImage result = copyImage(toPatch.getBufferedImage());
        //WritableImage result = new WritableImage(width, height);
        //getPixelWriter().setPixels(0,0,width,height,toPatch.getImage().getPixelReader(),0,0);

        for (Selection selection: selections)
        {
            Polygon res = selection.getBoundPixels();
            Polygon curr;
            while(res.npoints > 1)
            {
                curr = res;
                res = new Polygon();

                for (int i = 0; i < curr.npoints; i ++)
                {
                    ArrayList<Color> colorsToNormalise = new ArrayList<>();
                    ArrayList<Point> pointsToProcess = new ArrayList<>();

                    pointsToProcess.add(new Point(curr.xpoints[i] - 1, curr.ypoints[i]));
                    pointsToProcess.add(new Point(curr.xpoints[i] + 1, curr.ypoints[i]));
                    pointsToProcess.add(new Point(curr.xpoints[i], curr.ypoints[i] - 1));
                    pointsToProcess.add(new Point(curr.xpoints[i], curr.ypoints[i] + 1));

                    pointsToProcess.add(new Point(curr.xpoints[i] - 1, curr.ypoints[i] - 1));
                    pointsToProcess.add(new Point(curr.xpoints[i] - 1, curr.ypoints[i] + 1));
                    pointsToProcess.add(new Point(curr.xpoints[i] + 1, curr.ypoints[i] - 1));
                    pointsToProcess.add(new Point(curr.xpoints[i] + 1, curr.ypoints[i] + 1));

                    for (Point point: pointsToProcess)
                    {
                        if(!isPointAtBound(curr, point))
                            if(curr.contains(point))
                                addPoint(res, point);
                            else
                                colorsToNormalise.add(new Color(result.getRGB(point.x, point.y)));//getPixelReader().getColor(point.x, point.y));
                    }

                    result.setRGB(curr.xpoints[i], curr.ypoints[i], normalize(colorsToNormalise).getRGB()); //createGraphics().drawLine();
                }
            }
        }

        return result;
    }

    BufferedImage copyImage(BufferedImage source)
    {
        BufferedImage b = new BufferedImage(source.getWidth(), source.getHeight(), source.getType());
        Graphics2D g = b.createGraphics();
        g.drawImage(source, 0, 0, null);
        g.dispose();
        return b;
    }

    /**
     * Adds point to selected area
     * @param polygon polygon, to which the point is added
     * @param point point to be added
     */
    void addPoint(Polygon polygon, Point point)
    {
        if(!isPointAtBound(polygon,point))
            polygon.addPoint(point.x, point.y);
    }

    /**
     * Checks whether the point is inside polygon
     * @param polygon the polygon
     * @param point the point in question
     * @return true - point is inside, false - point is outside
     */
    boolean isPointAtBound(Polygon polygon, Point point)
    {
        for(int i = 0; i < polygon.npoints; i++)
        {
            if(polygon.xpoints[i] == point.x && polygon.ypoints[i] == point.y)
                return true;
        }
        return false;
    }

    /**
     * Normalises the color of a pixel with the surrounding pixels
     * @param colors the colors of the surrounding pixels
     * @return normalised color of a pixel
     */
    Color normalize(ArrayList<Color> colors)
    {
        int amount = colors.size();
        double red = 0, green = 0, blue = 0;
        for (Color color: colors)
        {
            red += color.getRed();
            green += color.getGreen();
            blue += color.getBlue();
        }
        red /= amount;
        green /= amount;
        blue /= amount;

        return new Color((int)Math.round(red),(int)Math.round(green),(int)Math.round(blue));
    }
}
