package logic;

import controller.MainWindowController;
import logic.exception.ImageException;
import logic.exception.ImageLoadException;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class MainWindow
{
    @Getter
    private MainWindowController controller;

    @Getter
    private ImageViewer imageViewer;

    @Setter @Getter
    private List<File> patchableImages;

    @Setter @Getter
    private File outputFolder;

    private List<ImageException> exceptions;

    public MainWindow() throws IOException
    {
        imageViewer = new ImageViewer();
        controller = MainWindowController.create(this);
        patchableImages = new ArrayList<>();
        exceptions = new ArrayList<>();
    }

    /**
     * Initiates patching of all selected images
     */
    public void patchAllSelectedImages()
    {
        this.controller.setBlockedState(true);

        new Thread(() ->
        {
            ImagePatcher imagePatcher = new ImagePatcher(getImageViewer().getSelections());

            if (outputFolder == null)
            {
                outputFolder = new File("output");
                outputFolder.mkdir();
            }

            ArrayList<Future<File>> results = FileProcessor.getInstance().processImages(patchableImages, outputFolder, getImageViewer().getPatchableImage(), imagePatcher);

            for(Future<File> result : results)
            {
                try
                {
                    result.get();
                    result = null;
                } catch (InterruptedException | ExecutionException e)
                {
                    if(e.getCause() instanceof ImageException)
                    {
                        ImageException e1 = (ImageException) e.getCause();
                        exceptions.add(e1);
                    }
                    else if (e.getCause() instanceof ImageLoadException)
                    {
                        //just ignored file
                    }
                }
            }
            System.gc();
            this.controller.setBlockedState(false);
        }).start();
    }

    /**
     * Initiates patching of an opened image
     */
    public void patchCurrentImage()
    {
        patchImage(getImageViewer().getPatchableImage(), getImageViewer().getPatchableImage());
    }

    /**
     * Initiates patching of an image
     * @param image Image to be patched
     * @param baseImage Image, opened in ImageViewer
     */
    private void patchImage(PatchableImage image, PatchableImage baseImage)
    {
        this.controller.setBlockedState(true);

        new Thread(() ->
        {
            ImagePatcher imagePatcher = new ImagePatcher(getImageViewer().getSelections());
            try
            {
                if (outputFolder == null)
                {
                    outputFolder = new File("output");
                    outputFolder.mkdir();
                }
                FileProcessor.getInstance().processImage(image.getSourceFile(), outputFolder, baseImage, imagePatcher).get();
            } catch (InterruptedException | ExecutionException e)
            {
                ImageException e1 = (ImageException) e.getCause();
                exceptions.add(e1);
            }
            finally
            {
                this.controller.setBlockedState(false);
            }
        }).start();
    }

    /**
     * Checks for thrown exceptions during patching process. If there are any - alert user
     */
    public void checkForExceptions()
    {
        if (exceptions.size() != 0)
            this.controller.alertUser(exceptions);
        this.exceptions = new ArrayList<>();
    }
}
