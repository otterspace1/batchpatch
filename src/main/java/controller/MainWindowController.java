package controller;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.util.Pair;
import logic.*;
import logic.exception.ImageDimensionException;
import logic.exception.ImageException;
import logic.exception.ImageLoadException;
import logic.exception.ImageSaveException;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.imaging.ImageReadException;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainWindowController
{
    /**
     * Creates controller for MainWindow
     * @param mainWindow MainWindow for which controller is created
     * @return controller for MainWindow
     * @throws IOException
     * @throws NullPointerException
     */
    public static MainWindowController create(MainWindow mainWindow) throws IOException, NullPointerException
    {
        FXMLLoader loader = new FXMLLoader();

        Pane pane = loader.load(MainWindowController.class.getResourceAsStream("/fxml/MainWindow.fxml"));
        MainWindowController controller = loader.getController();
        controller.setRootPane(pane);

        controller.setMainWindow(mainWindow);
        controller.setImageViewer(mainWindow.getImageViewer().getController());
        controller.setControlPane(ControlPaneController.create());

        return controller;
    }

    @FXML
    @Setter @Getter
    Pane rootPane;

    @FXML
    SplitPane splitPane;
    @FXML
    Pane leftPane;
    @FXML
    Pane rightPane;


    @Setter @Getter
    private MainWindow mainWindow;

    private ControlPaneController controlPaneController;

    @Getter @Setter
    private ImagePatcher imagePatcher;

    private BooleanProperty chooseFolderProperty;

    /**
     * Initializes components for MainWindow
     * @throws MalformedURLException
     */
    @FXML
    void initialize() throws MalformedURLException
    {
        this.chooseFolderProperty = new SimpleBooleanProperty();
        this.chooseFolderProperty.setValue(true);
    }

    /**
     * Sets ImageViewer in MainWindow
     * @param imageViewerController controller for ImageViewer, that needs to be set in MainWindow
     */
    void setImageViewer(ImageViewerController imageViewerController)
    {
        splitPane.getItems().set(0,mainWindow.getImageViewer().getController().getRootPane());
    }

    /**
     * Sets ControlPane in MainWindow
     * @param controlPaneController controller for ControlPane, that needs to be set in MainWindow
     */
    void setControlPane(ControlPaneController controlPaneController)
    {
        this.controlPaneController = controlPaneController;
        this.controlPaneController.getSelectionProperty().bindBidirectional(this.mainWindow.getImageViewer().getSelectionEnabledProperty());
        this.controlPaneController.getApplyProperty().setValue(event -> {
            this.mainWindow.getImageViewer().getController().stopBlinkTimer();
            this.mainWindow.patchAllSelectedImages();
        });
        this.controlPaneController.getChooseDirectoryProperty().bindBidirectional(this.chooseFolderProperty);
        this.controlPaneController.getAddFilesProperty().setValue(event -> addPatchableImages());
        this.controlPaneController.getOutputDirectoryProperty().setValue(event -> chooseOutputDirectory());
        this.controlPaneController.getRemoveSelectionsProperty().setValue(event -> {
            this.mainWindow.getImageViewer().removeSelections();
        });
        splitPane.getItems().set(1, this.controlPaneController.getRootPane());

        rootPane.widthProperty().addListener((observable, oldValue, newValue) ->
        {
            splitPane.setDividerPositions(this.controlPaneController.getRootPane().getWidth()/splitPane.getWidth());
        });
    }

    /**
     * Bind to File->Open. Opens image in program
     */
    @FXML
    void openImage()
    {
        File imageFile = chooseImageFile();
        if (imageFile != null)
        {
            try
            {
                mainWindow.getImageViewer().loadImageFromFile(imageFile);
            }
            catch (ImageLoadException e)
            {
                alertUser(e);
            }
            catch (IOException | ImageReadException e)
            {
                alertUser(new ImageLoadException(imageFile,"SOME MESSAGE"));
            }
        }
    }

    /**
     * Adds images for fix
     */
    private void addPatchableImages()
    {
        List<File> chosen = new ArrayList<>();
        if (this.chooseFolderProperty.get())
            chosen = chooseImageFiles(chooseDirectory("Папка исходных изображений..."));
        else
            chosen = chooseImageFiles();
        if (chosen != null)
        {
            this.mainWindow.setPatchableImages(chosen);
            this.controlPaneController.setList(this.mainWindow.getPatchableImages());
        }
    }

    /**
     * Chooses output directory for results
     */
    private void chooseOutputDirectory()
    {
        this.mainWindow.setOutputFolder(chooseDirectory("Папка с результатом..."));
        if (this.mainWindow.getOutputFolder() != null)
            this.controlPaneController.setOutputDirectoryText(this.mainWindow.getOutputFolder().getName());
    }

    /**
     * Chooses directory
     * @param title title for DirectoryChooser window
     * @return directory file
     */
    private File chooseDirectory(String title)
    {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle(title);
        return directoryChooser.showDialog(rootPane.getScene().getWindow());
    }

    /**
     * Adds files from directory to list of images for fix
     * @param directory directory file
     * @return list of files for fix
     */
    private List<File> chooseImageFiles(File directory)
    {
        List<File> files = new ArrayList<>();
        if (directory != null)
        {
            files.add(directory);
        }
        return files;
    }

    /**
     * Chooses several images for fix
     * @return list of files for fix
     */
    private List<File> chooseImageFiles()
    {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Исходные изображения...");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Изображние","*.jpeg", "*.jpg", "*.JPG", "*.JPEG"));
        return fileChooser.showOpenMultipleDialog(rootPane.getScene().getWindow());
    }

    /**
     * Chooses single image file to open
     * @return image file
     */
    private File chooseImageFile()
    {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Открыть изображение");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Изображние","*.jpeg", "*.jpg", "*.JPG", "*.JPEG"));
        return fileChooser.showOpenDialog(rootPane.sceneProperty().get().getWindow());
    }

    /**
     * Blocks/unblocks interface in time of fix
     * @param blocked block status of interface
     */
    public void setBlockedState(boolean blocked)
    {
        Platform.runLater(() ->
        {
            if(blocked)
                getRootPane().getScene().setCursor(Cursor.WAIT);
            else
                getRootPane().getScene().setCursor(Cursor.DEFAULT);
            splitPane.setDisable(blocked);
            if (!blocked)
            {
                mainWindow.checkForExceptions();
                mainWindow.getImageViewer().getController().startBlinkTimer();
            }
        });
    }

    /**
     * Generates alert for ImageLoadException
     * @param exception ImageLoadException that caused the alert
     */
    public void alertUser(ImageLoadException exception)
    {
        String message = exception.getImageFile().getName();
        Alert alert = new Alert(Alert.AlertType.ERROR, message);
        alert.setHeaderText(exception.getMessage());
        alert.showAndWait();
    }

    /**
     * Generates alerts for exceptions that occurred during fix
     * @param exceptions list of exceptions
     */
    public void alertUser(List<ImageException> exceptions)
    {
        HashMap<Class, Pair<StringBuilder, String>> exceptionMap = new HashMap<>();
        StringBuilder showMessage = new StringBuilder();
        for (ImageException e: exceptions)
        {
            if (exceptionMap.containsKey(e.getClass()))
                exceptionMap.get(e.getClass()).getKey().append(e.getExceptionImage().getName() + "\n");
            else
                exceptionMap.put(e.getClass(), new Pair(new StringBuilder(e.getExceptionImage().getName() + "\n"),e.getMessage()));
            showMessage.append(e.getExceptionImage().getName() + "\n");
        }
        Alert alert = null;
        for (Class cl: exceptionMap.keySet())
        {
            if (cl.equals(ImageSaveException.class))
            {
                alert = new Alert(Alert.AlertType.INFORMATION, exceptionMap.get(cl).getKey().toString());
                alert.setHeaderText(exceptionMap.get(cl).getValue());
                alert.showAndWait();
            }
            else if (cl.equals(ImageDimensionException.class))
            {
                alert = new Alert(Alert.AlertType.ERROR, exceptionMap.get(cl).getKey().toString());
                alert.setHeaderText(exceptionMap.get(cl).getValue());
                alert.showAndWait();
            }
        }
        showMessage.append(exceptions.get(0).getMessage());
    }

}
