package controller;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import logic.ImageViewer;
import logic.Selection;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.sun.javafx.util.Utils.clamp;

public class ImageViewerController
{
    /**
     * Creates controller for ImageViewer
     * @return controller for ImageViewer
     * @throws IOException
     * @throws NullPointerException
     */
    public static ImageViewerController create() throws IOException, NullPointerException
    {
        FXMLLoader loader = new FXMLLoader();

        Pane pane = loader.load(ImageViewerController.class.getResourceAsStream("/fxml/ImageViewer.fxml"));
        ImageViewerController controller = loader.getController();
        controller.setRootPane(pane);
        return controller;
    }

    @FXML
    @Setter @Getter
    private Pane rootPane;

    @Getter
    private ImageViewer imageViewer;
    private int zoomCounter;
    private double scaleCoefficient = 0.8;
    private ObjectProperty<Point2D> mouseDown = new SimpleObjectProperty<>();

    @FXML
    @Getter
    private PixelatedImageView imageView;

    private SimpleBooleanProperty selectionEnabledProperty;
    private Selection currSelection;
    @FXML
    private PixelatedImageView selectionImageView;
    private WritableImage selectionImage;

    private Timer selectionBlinkTimer;


    /**
     * Initializes components for ImageViewer
     */
    @FXML
    public void initialize()
    {
        selectionEnabledProperty = new SimpleBooleanProperty();
        imageView.setSmooth(false);

        imageView.fitWidthProperty().bind(rootPane.widthProperty());
        imageView.fitHeightProperty().bind(rootPane.heightProperty());

        selectionImageView.setSmooth(false);
        selectionImageView.fitWidthProperty().bind(imageView.fitWidthProperty());
        selectionImageView.fitHeightProperty().bind(imageView.fitHeightProperty());
    }

    /**
     * Initializes ImageViewer: sets up event handlers and binds with image
     * @param imageViewer ImageViewer for initialization
     */
    public void setImageViewer(ImageViewer imageViewer)
    {
        this.imageViewer = imageViewer;
        this.imageView.imageProperty().bindBidirectional(imageViewer.getImage());
        this.imageViewer.getImage().addListener((observable, oldValue, newValue) -> resetPresentation(newValue));
        this.selectionImageView.setOnScroll(e -> {
            if (imageView.getImage() != null)
            {
                double delta = e.getDeltaY();
                Point2D coordinates = new Point2D(e.getX(), e.getY());
                if (delta > 0)
                    zoomIn(coordinates);
                else
                    zoomOut(coordinates);
            }
        });

        this.selectionImageView.setOnMousePressed(e -> {
            if (imageView.getImage() != null)
            {
                Point2D mousePress = mapImageViewToImage(imageView, new Point2D(e.getX(), e.getY()));
                mouseDown.set(mousePress);
            }
        });

        this.selectionImageView.setOnMouseDragged(e -> {
            if (imageView.getImage() != null)
            {
                if (!selectionEnabledProperty.getValue())
                {
                    if (zoomCounter > 0)
                        shift(new Point2D(e.getX(), e.getY()));
                } else
                {
                    addPointToSelection(mapImageViewToImage(imageView, new Point2D(e.getX(), e.getY())));
                }
            }
        });

        this.selectionImageView.setOnMouseReleased(event ->
        {
            if (imageView.getImage() != null)
            {
                if (selectionEnabledProperty.getValue())
                {
                    finishSelection();
                }
            }
        });

        this.selectionEnabledProperty.bindBidirectional(this.imageViewer.getSelectionEnabledProperty());

        if(this.imageViewer.getPatchableImage() != null)
        {
            this.imageView.setRotate(this.imageViewer.getPatchableImage().getImageRotationDegrees());
            this.selectionImageView.setRotate(this.imageViewer.getPatchableImage().getImageRotationDegrees());
        }
    }

    /**
     * Calculates zoomCounter for zooming in
     * @param coordinates coordinates of a point on ImageViewer, on which to zoom in
     */
    private void zoomIn(Point2D coordinates)
    {
        zoomCounter++;
        zoom(scaleCoefficient, coordinates);
    }

    /**
     * Calculates zoomCounter and scaleCoefficient for zoming out
     * @param coordinates coordinates of a point on ImageViewer, on which to zoom out
     */
    private void zoomOut(Point2D coordinates)
    {
        zoomCounter--;
        if (zoomCounter >= 0)
            zoom(1/ scaleCoefficient, coordinates);
        if (zoomCounter == -1)
            zoomCounter = 0;
    }

    /**
     * Zooms image in ImageViewer
     * @param scale multiplier for scaling operation
     * @param coordinates coordinates of a point on ImageViewer, on which to zoom
     */
    private void zoom(double scale, Point2D coordinates)
    {
        Point2D mouse = mapImageViewToImage(imageView, coordinates);
        Rectangle2D viewport = imageView.getViewport();
        double width = imageView.getImage().getWidth();
        double height = imageView.getImage().getHeight();
        double newWidth = viewport.getWidth() * scale;
        double newHeight = viewport.getHeight() * scale;
        double newMinX = 0;
        double newMinY = 0;

        if (newWidth < width)
        {
            newMinX = clamp(mouse.getX() - (mouse.getX() - viewport.getMinX()) * scale,
                    0, width - newWidth);
        }
        if (newHeight < height)
        {
            newMinY = clamp(mouse.getY() - (mouse.getY() - viewport.getMinY()) * scale,
                    0, height - newHeight);
        }

        imageView.setViewport(new Rectangle2D(newMinX, newMinY, newWidth, newHeight));

        selectionImageView.setViewport(new Rectangle2D(newMinX, newMinY, newWidth, newHeight));
    }

    /**
     * Moves image in ImageViewer
     * @param coordinates coordinates of a point on ImageViewer from which to move
     */
    private void shift(Point2D coordinates)
    {
        coordinates = mapImageViewToImage(imageView, coordinates);
        Point2D delta = coordinates.subtract(mouseDown.get());
        Rectangle2D viewport = imageView.getViewport();
        double width = imageView.getImage().getWidth();
        double height = imageView.getImage().getHeight();
        double maxX = width - viewport.getWidth();
        double maxY = height - viewport.getHeight();
        double minX = clamp(viewport.getMinX() - delta.getX(), 0, maxX);
        double minY = clamp(viewport.getMinY() - delta.getY(), 0, maxY);
        if (minX < 0.0)
        {
            minX = 0.0;
        }
        else if (minX + viewport.getWidth() > width)
        {
            minX = viewport.getMinX();
        }
        if (minY < 0.0)
        {
            minY = 0.0;
        }
        else if (minY + viewport.getHeight() > height)
        {
            minY = viewport.getMinY();
        }
        imageView.setViewport(new Rectangle2D(minX, minY, viewport.getWidth(), viewport.getHeight()));

        selectionImageView.setViewport(new Rectangle2D(minX, minY, viewport.getWidth(), viewport.getHeight()));
    }

    /**
     * Recalculates point coordinates in Image coordinate system
     * @param imageView ImageView, in which Image is displayed
     * @param imageViewCoordinates coordinates in ImageView coordinate system
     * @return point coordinates in Image coordinate system
     */
    private Point2D mapImageViewToImage(ImageView imageView, Point2D imageViewCoordinates)
    {
        double xProportion = imageViewCoordinates.getX() / imageView.getBoundsInLocal().getWidth();
        double yProportion = imageViewCoordinates.getY() / imageView.getBoundsInLocal().getHeight();

        Rectangle2D viewport = imageView.getViewport();
        return new Point2D(
                viewport.getMinX() + xProportion * viewport.getWidth(),
                viewport.getMinY() + yProportion * viewport.getHeight());
    }

    /**
     * Resets ImageView when the new image is opened
     * @param newImage
     */
    private void resetPresentation(Image newImage)
    {
        this.imageView.setViewport(new Rectangle2D(0, 0, newImage.getWidth(), newImage.getHeight()));
        this.imageView.setScaleX(1.0);
        this.imageView.setScaleY(1.0);
        this.zoomCounter = 0;

        selectionImage = new WritableImage((int) Math.round(imageViewer.getImage().getValue().getWidth()), (int) Math.round(imageViewer.getImage().getValue().getHeight()));
        this.selectionImageView.setImage(selectionImage);
        this.selectionImageView.setViewport(new Rectangle2D(0, 0, newImage.getWidth(), newImage.getHeight()));
        this.selectionImageView.setScaleX(1.0);
        this.selectionImageView.setScaleY(1.0);

        this.imageView.setRotate(this.imageViewer.getPatchableImage().getImageRotationDegrees());
        this.selectionImageView.setRotate(this.imageViewer.getPatchableImage().getImageRotationDegrees());
    }

    /**
     * Starts new Selection as current selection.     *
     * @param point First point of new Selection.
     */
    private void startSelection(Point2D point)
    {
        if (currSelection != null)
            finishSelection();
        currSelection = new Selection(point);
    }

    /**
     * Adds specified point to currently active selection and draws new selection segment on selectionImage.          *
     * @param point 2D-point to be added, in image coordinates
     */
    private void addPointToSelection(Point2D point)
    {
        if (currSelection != null)
        {
            drawNewEdge(selectionImage, Color.WHITE, Color.BLACK, currSelection.addPoint(point));
        }
        else
        {
            startSelection(point);
        }
    }

    /**
     * Finalises current selection, so currSelection can be used for handling the new Selection.
     */
    private void finishSelection()
    {
        if (currSelection != null)
        {
            drawNewEdge(selectionImage, Color.WHITE, Color.BLACK, currSelection.closeLine());
            imageViewer.getSelections().add(currSelection);
            currSelection = null;
        }
        if (selectionBlinkTimer == null)
            startBlinkTimer();
    }

    /**
     * Draws new edge in selection contour
     * @param g image, on which to draw
     * @param firstColor first color to draw in an edge
     * @param secondColor second color to draw in an edge
     * @param edge coordinates of the edge
     */
    private void drawNewEdge(WritableImage g, Color firstColor, Color secondColor, List<Point2D> edge)
    {
        Color currColor = firstColor;
        for (Point2D point2D : edge)
        {
            g.getPixelWriter().setColor((int) point2D.getX(), (int) point2D.getY(), currColor);

            if (currColor.equals(firstColor))
                currColor = secondColor;
            else
                currColor = firstColor;
        }
    }

    /**
     * Blinks the outline of the selection
     * @param g Image, on top of which selection was drawn
     * @param firstColor first color of selection
     * @param secondColor second color of selection
     * @param selection selected outline
     */
    private void blinkSelection(WritableImage g, Color firstColor, Color secondColor, Selection selection)
    {
        int amount = selection.getBoundPixels().npoints;

        Color currColor;
        int x, y;
        for(int i = 0; i < amount; i++)
        {
            x = selection.getBoundPixels().xpoints[i];
            y = selection.getBoundPixels().ypoints[i];
            currColor = g.getPixelReader().getColor(x,y);

            if (currColor.equals(firstColor))
                currColor = secondColor;
            else
                currColor = firstColor;
            g.getPixelWriter().setColor(x, y, currColor);
        }
    }

    /**
     * Removes selections from selectionImage
     */
    public void removeSelections()
    {
        if (imageView.getImage() != null)
        {
            selectionImage = new WritableImage((int) Math.round(imageViewer.getImage().getValue().getWidth()), (int) Math.round(imageViewer.getImage().getValue().getHeight()));
            this.selectionImageView.setImage(selectionImage);
            this.selectionImageView.setViewport(imageView.getViewport());
            this.selectionImageView.setScaleX(imageView.getScaleX());
            this.selectionImageView.setScaleY(imageView.getScaleY());
        }
    }

    /**
     * Stops timer, so the selection doesn't blink
     */
    public void stopBlinkTimer()
    {
        if (selectionBlinkTimer != null)
        {
            selectionBlinkTimer.cancel();
            selectionBlinkTimer = null;
        }
    }

    /**
     * Starts timer, so the selection blinks
     */
    public void startBlinkTimer()
    {
        selectionBlinkTimer = new Timer();
        selectionBlinkTimer.scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run()
            {
                Platform.runLater(() ->
                {
                    for (Selection selection : imageViewer.getSelections())
                    {
                        //Platform.runLater(() -> blinkSelection(selectionImage, Color.WHITE, Color.BLACK, selection));
                        blinkSelection(selectionImage, Color.WHITE, Color.BLACK, selection);
                    }
                });
            }
        },0,500L);
    }
}
