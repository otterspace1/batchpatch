package controller;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.Pane;
import lombok.Getter;
import lombok.Setter;
import org.controlsfx.control.ToggleSwitch;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class ControlPaneController
{
    @FXML
    @Setter @Getter
    private Pane rootPane;

    @FXML
    private ToggleButton selectionButton;

    @FXML
    private Button applyButton;

    @FXML
    private Button addFilesButton;

    @FXML
    private Button removeSelections;

    @FXML
    private ToggleSwitch directoryButton;

    @FXML
    private Button outputDirectoryButton;

    @FXML
    private TextField outputDirectoryText;

    @FXML
    private ListView<String> filesListView;

    @Getter
    private BooleanProperty selectionProperty;

    @Setter @Getter
    private Property<EventHandler<ActionEvent>> applyProperty;

    @Setter @Getter
    private Property<EventHandler<ActionEvent>> addFilesProperty;

    @Setter @Getter
    private BooleanProperty chooseDirectoryProperty;

    @Setter @Getter
    private Property<EventHandler<ActionEvent>> outputDirectoryProperty;

    @Setter @Getter
    private Property<EventHandler<ActionEvent>> removeSelectionsProperty;


    /**
     * Creates ControlPane controller
     * @return controller for ControlPane
     * @throws IOException
     * @throws NullPointerException
     */
    public static ControlPaneController create() throws IOException, NullPointerException
    {
        FXMLLoader loader = new FXMLLoader();

        Pane pane = loader.load(ControlPaneController.class.getResourceAsStream("/fxml/ControlPane.fxml"));
        ControlPaneController controller = loader.getController();
        controller.setRootPane(pane);

        return controller;
    }

    /**
     * Initializes components for ControlPane
     */
    @FXML
    void initialize()
    {
        selectionProperty = new SimpleBooleanProperty();
        selectionButton.selectedProperty().bindBidirectional(selectionProperty);
        selectionProperty.addListener((observable, oldValue, newValue) ->
        {
            if(newValue)
                applyButton.setDisable(true);
            else
                applyButton.setDisable(false);
        });

        applyProperty = new SimpleObjectProperty<>();
        applyButton.onActionProperty().bindBidirectional(applyProperty);

        addFilesProperty = new SimpleObjectProperty<>();
        addFilesButton.onActionProperty().bindBidirectional(addFilesProperty);

        chooseDirectoryProperty = new SimpleBooleanProperty();
        directoryButton.selectedProperty().bindBidirectional(chooseDirectoryProperty);

        outputDirectoryProperty = new SimpleObjectProperty<>();
        outputDirectoryButton.onActionProperty().bindBidirectional(outputDirectoryProperty);

        outputDirectoryText.setDisable(true);

        removeSelectionsProperty = new SimpleObjectProperty<>();
        removeSelections.onActionProperty().bindBidirectional(removeSelectionsProperty);
    }

    /**
     * Displays the list of opened files
     * @param files list of opened files, that needs to be displayed
     */
    public void setList(List<File> files)
    {
        List<String> fileNames = files.stream().map(f -> f.getName()).collect(Collectors.toList());
        filesListView.setItems(FXCollections.observableArrayList(fileNames));
    }

    /**
     * Displays the name of output directory
     * @param text name of the output folder
     */
    public void setOutputDirectoryText(String text)
    {
        outputDirectoryText.setText(text);
    }
}
