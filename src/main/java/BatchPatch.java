import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import logic.FileProcessor;
import logic.MainWindow;

import java.io.IOException;
import java.util.Optional;

public class BatchPatch extends Application
{

    public static void main(String[] args)
    {
        launch(args);
    }

    /**
     * Starts the program
     * @param primaryStage main stage
     */
    @Override
    public void start(Stage primaryStage)
    {
        try
        {
            MainWindow mainWindow = new MainWindow();

            primaryStage.setScene(new Scene(mainWindow.getController().getRootPane()));
            primaryStage.setTitle("BatchPatch");
            primaryStage.setOnCloseRequest(event ->
            {
                try
                {
                    if(!FileProcessor.getInstance().deinitialise())
                    {
                        if(showTerminatingDialog())
                            FileProcessor.getInstance().deinitializeForcibly();
                    }
                } catch (InterruptedException e)
                {
                    if(showTerminatingDialog())
                        FileProcessor.getInstance().deinitializeForcibly();
                }
                mainWindow.getImageViewer().getController().stopBlinkTimer();
            });

            primaryStage.show();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Alerts user if there was an exception during the start of the program
     * @return result of the alert
     */
    private boolean showTerminatingDialog()
    {
        Alert alert = new Alert(Alert.AlertType.WARNING, "Обработка не завершена, прервать выполнение?");
        Optional<ButtonType> res = alert.showAndWait();
        return res.map(buttonType -> buttonType.getButtonData().isDefaultButton()).orElse(false);
    }
}
